"""
Calculation of Fibonacci's row element
"""

import sys 				# importing tools for console

n = int(sys.argv[1])	# reading element's number
result = 0 				# 1 resulting value
result_prev = 1			# 0 resulting value of previous step

"""
if n==0:				# if number = 0 than it's zero
	print '0'
	exit()
"""
for i in range(n):		# n-1
	result = result + result_prev
	result_prev = result-result_prev
print result
