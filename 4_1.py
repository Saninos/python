import sys

x = str(sys.argv[1]).lower()
y = list(x)

if len(y) == 1:
	print 'YES'
	exit()

result = []

i = 0

for i in y:
	if i <> ' ':
		result.append(i)

y = result

is_polindrom = False

for i in range(len(y)/2):
	if y[i] == y[(len(y)-1)-i]:
		is_polindrom = True
	else:
		is_polindrom = False
		break

if is_polindrom:
	print 'YES'
else:
	print 'NO'