import sys

inp = str(sys.argv[1])
counter = 0

for elem in inp:
	if counter >= 0:
		if elem == '(': 
			counter += 1
		else:
			counter -= 1
	else:
		print 'NO'
		exit()

if counter == 0:
	print 'YES'
else: print 'NO'
