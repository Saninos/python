import sys

#variables initialization
i = 0
j = 0
count = 0
suma = 0
sumb = 0
lucky_count = 0

arg1 = list(sys.argv[1])
arg2 = list(sys.argv[2])

#checking if lenght of arguments is less than 6, adding '0'
#before number
if len(arg1) < 6:
		for k in range(6-len(arg1)):
			arg1.insert(0,'0')

if len(arg2) < 6:
		for k in range(6-len(arg2)):
			arg2.insert(0,'0')

beg_number = ''.join(arg1)
end_number = ''.join(arg2)

#counting number of steps between beginnig and end
count = int(end_number) - int(beg_number) + 1
prom_number = list(beg_number)

#main loop
for i in range(count):
	a = prom_number[:3]		#parsing current number into two parts
	b = prom_number[3:]
	suma = 0				#initializating sums
	sumb = 0
	for j in range(3):
		suma = suma + int(a[j]) #calculating sum of each part
		sumb = sumb + int(b[j])

	if suma == sumb: 		#if sums are equal then we have lucky number!
		lucky_count += 1
	
	prdif = int(''.join(map(str,prom_number)))+1 #converting current number to int and incrementing it
	prom_number = map(int, str(prdif))			#converting back to list
	if len(prom_number) < 6:					#checking lenght of list and adding '0' before number if necessary
		for k in range(6-len(prom_number)):
			prom_number.insert(0,0)

print lucky_count						#printing result

