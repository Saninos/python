import sys

inp = str(sys.argv[1]).replace(" ", "")

splits = []
splits2 = []
key = 'aaaaabbbbbabbbaabbababbaaababaab'
alphabet = 'abcdefghijklmnopqrstuvwxyz'
answ = ''

for i in range(0,len(inp),5):
	splits.append(inp[i:i+5])

for i in range(len(splits)):
	if len(splits[i]) < 5:
		splits.pop(i)

for elem in splits:
	elems = ''
	for i in range(len(elem)):
		if elem[i].islower():
			elems += 'a'
		else:
			elems += 'b'
	splits2.append(elems)
	
for elem in splits2:
	for j in range(len(key)):
		if elem == key[j:j+5]:
			answ += alphabet[j]

print answ