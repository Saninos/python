def file_search(folder, filename):
	path = folder[0]
	for i in folder:
		if isinstance(i,list):
			subpath = file_search(i, filename)
			if subpath != False:
				return path + '/' + str(subpath)
		if i == filename:
			return path + '/' + i
	if path == folder[0]:
			return False

print(file_search(
	[ 
		'/home', 
		['user1'], 
		[
			'user2', 
			['my pictures'], 
			[
				'desktop', 
				'not this', 
				'and not this', 
				[
					'new folder', 
					'hereiam.py'
				] 
			] 
		], 
		'work.ovpn', 
		'prometheus.7z', 
		[
			'user3', 
			['temp'], 
		], 
		'hey.py'
	], 
	'hereiam.py')
)