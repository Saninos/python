def count_holes(n):
	if isinstance(n,float):
		return 'ERROR'
	try:
		m = str(int(n))
	except (ValueError, TypeError):
		return 'ERROR'
	count = 0
	for i in m:
		if i == '9' or i == '6' or i == '4' or i == '0':
			count +=1
		if i == '8':
			count +=2
	return count

print count_holes(888)