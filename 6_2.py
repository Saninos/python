def encode_morze(text):
	morse_code = {"A" : ".-", "B" : "-...", "C" : "-.-.", "D" : "-..", "E" : ".", "F" : "..-.", 
	"G" : "--.", "H" : "....", "I" : "..", "J" : ".---", "K" : "-.-", "L" : ".-..", "M" : "--", 
	"N" : "-.", "O" : "---", "P" : ".--.", "Q" : "--.-", "R" : ".-.", "S" : "...", "T" : "-", 
	"U" : "..-", "V" : "...-", "W" : ".--", "X" : "-..-", "Y" : "-.--", "Z" : "--.."}
	result_dots = ''
	letter_shift = ' '*3
	word_shift = ' '*4
	flag = True
	result = ''
	text_to_code = str(text.upper())
	if text_to_code[len(text_to_code)-1] == '.':
		text_to_code =  text_to_code[:-1]
	for i in range(len(text_to_code)):
		flag = True
		for letter in morse_code:
			if text_to_code[i] == letter:
				result_dots += ' '.join(morse_code[letter])
				if i != len(text_to_code)-1:
					result_dots +=  letter_shift
			if text_to_code[i] == ' ' and flag:
				result_dots += word_shift
				flag = False
	for symbol in result_dots:
		if symbol == '.':
			result += '^'
		elif symbol == ' ':
			result += '_'
		elif symbol == '-':
			result += '^'*3 

	return result


print encode_morze('sos.')