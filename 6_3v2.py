def saddle_point(matrix):

	same_row = [False] * len(matrix)
	same_col = [False] * len(matrix[0])

	min_row_list = []
	max_col_list = []
	point = ()

	for i in range(len(matrix)):
		min_coords = (i,0)
		added = False
		for j in range(len(matrix[0])):
			if matrix[i][j] < matrix[min_coords[0]][min_coords[1]]:
				min_coords = (i,j)
			elif matrix[i][j] == matrix[min_coords[0]][min_coords[1]] and j != 0 and added != True:
				same_row[i] = True
				added = True
		min_row_list.append(min_coords)
	print min_row_list, same_row

	for j in range(len(matrix[0])):
		max_coords = (0,j)
		added = False
		for i in range(len(matrix)):
			if matrix[i][j] > matrix[max_coords[0]][max_coords[1]]:
				max_coords = (i,j)
			elif matrix[i][j] == matrix[max_coords[0]][max_coords[1]] and i != 0 and added != True:
				same_col[i] = True
				added = True
				print j, matrix[i][j], matrix[max_coords[0]][max_coords[1]], max_coords
		max_col_list.append(max_coords)
	
	print max_col_list, same_col

	point = set(min_row_list) & set(max_col_list)
	try:
		point_t = next(iter(point))
	except StopIteration:
		return False
		
	if len(point_t) == 0:
		return False

	if same_row[point_t[0]] == True or same_col[point_t[1]] == True:
		return False
	else:
		return point_t
	

print saddle_point([[5,5,5], [5,5,6], [5,4,5]])