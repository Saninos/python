#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# The above encoding declaration is required and the file must be saved as UTF-8


"""Process ads link.

Process links from sites with car ads in order to return information in desired
format.
"""


import requests
from bs4 import BeautifulSoup
from pprint import pprint
from html.parser import HTMLParser
from fnmatch import fnmatch

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(str(html))
    return s.get_data()


def get_page(link):
    """Get the page following link."""
    response = requests.get(link)
    if response.status_code == 200:
        return BeautifulSoup(response.text, 'html.parser')
    else:
        return 'Error'


def parse_meta(link):
    """Parse meta data in order to get main information."""
    parsed_data = {}
    if link is not None or link != '':
        soup = get_page(link)
        if soup is not 'Error':
            if fnmatch(link, '*rst.ua*'):
                meta = soup.find('meta', {'name': 'keywords'})
                meta_clean = str(meta)[15:str(meta).find(' name=') - 1]
                specs = meta_clean[:meta_clean.find('Технические характеристики')]
                specs_list = specs.split(',')
                if fnmatch(specs_list[1], '*обмен*'):
                    parsed_data['car_name'] = specs_list[0][specs_list[0].find('Продам') + len('Продам '):]
                    parsed_data['price'] = specs_list[2][4:]
                    parsed_data['year'] = specs_list[3][1:5]
                    parsed_data['engine'] = specs_list[4][1:]
                    parsed_data['city'] = specs_list[5][12:-2]
                else:
                    parsed_data['car_name'] = specs_list[0][specs_list[0].find('Продам')+len('Продам '):]
                    parsed_data['price'] = specs_list[1][4:]
                    parsed_data['year'] = specs_list[2][1:5]
                    parsed_data['engine'] = specs_list[3][1:]
                    parsed_data['city'] = specs_list[4][12:-2]
            elif fnmatch(link, '*auto.ria*'):
                meta = soup.find('meta', {'name': 'description'})
                meta_title = soup.find('meta', {'name': 'title'})
                tech_specs = soup.find('div', {'class': 'technical-characteristics line-argument'})
                meta_str = str(meta)
                meta_clean = meta_str[meta_str.find('AUTO.RIA') + len('AUTO.RIA '):
                                      - meta_str.find('AUTO.RIA') - len('на AUTO.')]
                specs = meta_clean.split(', ')
                parsed_data['car_name'] = ' '.join(specs[0].split(' ')[:2])
                parsed_data['year'] = ' '.join(specs[0].split(' ')[-2:-1])
                for spec in specs:
                    if fnmatch(spec, '*двигатель*'):
                        parsed_data['engine'] = spec[spec.find(' ') + 1:]
                    if fnmatch(spec, '*пробег*'):
                        parsed_data['mileage'] = ' '.join(spec.split(' ')[1:3])
                meta_title_clean = str(meta_title).split(' ')[-3:-1]
                parsed_data['price'] = meta_title_clean[0][:-1]
                parsed_data['city'] = meta_title_clean[1][:-1]
                tech_specs_clean = tech_specs.find_all('span', {'class': 'argument'})
                for spec in tech_specs_clean:
                    if fnmatch(str(spec), '*Автомат*') and len(str(spec)) < 50:
                        parsed_data['gearshaft'] = strip_tags(spec)
                    elif fnmatch(str(spec), '*Механика*') and len(str(spec)) < 50:
                        parsed_data['gearshaft'] = strip_tags(spec)
                    elif fnmatch(str(spec), '*Типтроник*') and len(str(spec)) < 50:
                        parsed_data['gearshaft'] = strip_tags(spec)

            try: 
                parsed_data['engine']
                parsed_data['gearshaft']
                parsed_data['mileage']
            except KeyError:
                parsed_data['engine'] = ' - '
                parsed_data['gearshaft'] = ' - '
                parsed_data['mileage'] = ' - '
            return parsed_data
        else:
            print('Error in parsing specs')
    return parsed_data

def parse_contacts(link):
    """Parse contacts information."""
    parsed_data = {}
    soup = get_page(link)
    if soup is not 'Error':
        if fnmatch(link, '*rst.ua*'):
            contacts = soup.find_all('div', {'class': 'rst-page-oldcars-item-option-block-container'})
            contacts_nohtml = strip_tags(contacts[1])
            contacts_clean = contacts_nohtml.split('§')
            parsed_data['name'] = contacts_clean[0]
            for i, tel in enumerate(contacts_clean[1:]):
                tel_name = 'tel' + str(i + 1)
                parsed_data[tel_name] = "'" + tel[6:]
            return parsed_data
        elif fnmatch(link, '*auto.ria*'):
            contacts_name = soup.find('dt', {'class': 'user-name'})
            contacts_name_clean = strip_tags(str(contacts_name))
            parsed_data['name'] = ''.join(contacts_name_clean.split('§'))
            contacts_tels = soup.find_all('div', {'class': 'phone'})
            for i, tel in enumerate(contacts_tels):
                number = strip_tags(tel).split('§')[1]
                tel_name = 'tel' + str(i + 1)
                parsed_data[tel_name] = "'" + number
            return parsed_data

def process_links(links):
    """Process link and print information in desired format."""
    for link in links:
        if fnmatch(link, '*rst.ua*') or fnmatch(link, '*auto.ria*'):
            try:
                data_meta = parse_meta(link)
                contacts = parse_contacts(link)
                print(link, end='\t')
                print(data_meta['car_name'],
                      data_meta['price'],
                      data_meta['year'],
                      data_meta['engine'],
                      data_meta['city'],
                      data_meta['mileage'],
                      data_meta['gearshaft'],
                      contacts['name'],
                      sep='\t',
                      end='\t'
                      )
                for key in contacts:
                    if key is not 'name':
                        print(contacts[key], end='\t')
                print()
            except IndexError:
                print('ERROR! Check the link manually')

links_str = '''https://auto.ria.com/auto_ford_focus_19392773.html
'''
links = links_str.splitlines()
process_links(links)
