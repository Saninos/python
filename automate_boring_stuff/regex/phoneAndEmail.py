#! python3
# phoneAndEmail.py - Finds phone numbers and email addresses on the clipboard.

import pyperclip
import re

phoneRegex = re.complie(r'''(
	(\d{3}|\(\d{3}\))? 			# mobile operator, e.g. ''050''
	(\s|-|\.)?					# separator
	(\d{3}|\(\d{3}\)) 			# first 3 digits
	((\s|-|\.)					# separator and two digits are groupped
	(\d{2})){2}					# to match somethin like ''-22-24''
	)''', re.VERBOSE)

# TODO: Create email regex.

# TODO: Find matches in clipboard text.

# TODO: Copy results to the clipboard.