import requests, json, html
from bs4 import BeautifulSoup
from pprint import pprint
from openpyxl import Workbook, load_workbook

dataIDs = {
    'Sku' : 'Sku_Result',
    'AltCats' : 'AltCats_Result',
    'Avus' : 'Avus_Result',
    'Specs' : 'Specs_Result',
    'Texts' : 'Texts_Result',
    'Images' : 'Images_Result',
    'Has' : 'Has_Result'
}

#parses recieved response into json
def load_data(response):
    #response = requests.get('http://templex.cnetcontent.com/Tps')
    soup = BeautifulSoup(response.text,'html.parser')
    result = soup.find_all('textarea')
    return result

#gets a specified part of loaded data
def get_data(data,part):
    for area in data:
        if area.attrs['id'] == part and area.attrs['id'].find(part) not in (None,[]):
            partData = json.loads(area.text)
    return partData

#returns ready-to-use string for POST request
def set_parameters(customerId, customerPn):
    parameters = customerId + 'customerPn=' + str(customerPn)
    #print(parameters)
    return {'Parameters' : parameters}

def get_sku_ID_column(worksheet):
    row1 = list(worksheet.rows)[0]
    IDCell = ''
    idcounter = 0
    for cell in row1:
        if str(cell.value) in ('Cust_SKU'):
            IDCell = cell
            idcounter += 1
    return [IDCell, idcounter]

def get_sku_list(worksheet, SKUcolumn):
    skuList = []
    for cell in worksheet[SKUcolumn.column]:
        if cell.value is not None: 
            #if cell.value.find('s'): 
            #   skuList.append(cell.value[1:]) #adds sku to list, without starting S
            #else:
                skuList.append(cell.value)
    return skuList[1:] #skips column title

path = 'C:\\Users\\sera\\Google Drive\\worten\\11569_FullDump_2016-10-20.xlsx'
wb = load_workbook(path)
ws = wb.active
#skuColumn = ws.get_column(0)
customerId = 'customerId=11569' + '&'
#customerPnUnparsed = '''5521723'''
customerPn = []
for cell in list(ws.columns)[0][1:100]:
    customerPn.append(cell.value)

i = 1
for sku in customerPn:
    #setting POST request parameters
    url = 'http://txdev1.cnetcontent.com/Tps'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = set_parameters(customerId, sku)
    print(sku)
    #sending POST request
    response = requests.post(url=url, headers=headers, data=data)
    print(i,':',response.status_code)
    if response.status_code == 200:
        data = load_data(response)
        if len(data)>0:
            skuData = get_data(data,dataIDs['Sku']) #loading alternative category data
            hasData = get_data(data,dataIDs['Has']) #loading has data
            skuDataError = [s for s in skuData['debug'] if 'Block ''SkuDataApiBlock'' was excluded' in s]
            if len(skuDataError) >0 and len(hasData)>0: print(sku,'\n',skuDataError,'\n','mappingStatusId=',hasData[0]['mappingStatusId'])
        i += 1