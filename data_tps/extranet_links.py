"""Generates extranet links.

Accepts SKUs and returns extranet links for them

"""

import requests
from bs4 import BeautifulSoup
from pprint import pprint


def set_parameters(sku):
    """Return ready-to-use data string for POST request."""
    parameters = 'ctl00$ctl00$RightPlaceHolder$RightPlaceHolder$tbSystemID:12345679' + sku
    return {'Parameters': parameters}


link = 'https://extranet.cnetcontentsolutions.com/sku-availability.aspx'

skus_excel = '''`12345679
'''
skus = skus_excel.splitlines()

for sku in skus:
    data = set_parameters(sku)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    s = requests.Session()
    s.auth = ('aserebrov', 'yG38wFi2')
    s.headers.update(headers)
    resp = s.get(url=link, stream=True)
    print(resp.text)
    '''response = r.post(url=link, headers=headers, data=data)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        #print(soup)'''