# -*- coding: utf-8 -*-

import requests
import sys

def googleSearch(query):
    with requests.session() as c:
        url = 'https://www.google.com'
        query = {'q': query}
        urllink = requests.get(url, params=query)
        print(urllink.url, urllink.text.encode(sys.stdout.encoding, errors='replace'))

googleSearch('1234342349')