"""Returns HAS data for listed SKUs."""

import requests
import json
from bs4 import BeautifulSoup

dataIDs = {
    'Sku': 'Sku_Result',
    'AltCats': 'AltCats_Result',
    'Avus': 'Avus_Result',
    'Specs': 'Specs_Result',
    'Texts': 'Texts_Result',
    'Images': 'Images_Result',
    'Has': 'Has_Result'
}


def load_data(response):
    """Parse recieved response into json."""
    soup = BeautifulSoup(response.text, 'html.parser')
    result = soup.find_all('textarea')
    return result


def get_data(data, part):
    """Get a specified part of loaded data."""
    for area in data:
        if area.attrs['id'] == part:
            part_data = json.loads(area.text)
    return part_data


def set_parameters(sku):
    """Return ready-to-use string for POST request."""
    parameters = customerID + 'customerPn=' + sku
    return {'Parameters': parameters}

skus_excel = '''4243074
4254911
4254912
3839853
4226137
4302883
3825523
4242567
3964541
3825544
3963021
4165962
4183682
4226141
4242568
4242571
2559530
4194575
3824788
4955721
2853707
3726973
4721806
3824845
3804602
4721807
3824800
2559535
2847881
4302882
4165753
4721808
4302884
4226136
3824792
4302881
3839936
3824791
5939806
5939804
5939805
'''
skuList = skus_excel.splitlines()

customerID = 'customerId=11569&'
sid_list = []

for i, sku in enumerate(skuList):
    # Set POST request parameters
    url = 'http://txdev1.cnetcontent.com/Tps'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = set_parameters(sku)

    # sending POST request
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code == 200:
        data = load_data(response)
        try:
            hasData = get_data(data, dataIDs['Has'])
            print(i + 1, '/', len(skuList), hasData)
            if len(hasData) > 0:
                sid_list.append(hasData[0]['id'])
        # appears if HAS data is empty
        except json.decoder.JSONDecodeError:
            print(i + 1)

for sid in sid_list:
    print(sid)
