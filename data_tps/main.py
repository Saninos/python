import requests, json, html
from bs4 import BeautifulSoup
from pprint import pprint
import configparser, os
from preprocess.request import main as preproc_skus
from pymongo import MongoClient

dataIDs = {
	'Sku' : 'Sku_Result',
	'AltCats' : 'AltCats_Result',
	'Avus' : 'Avus_Result',
	'Specs' : 'Specs_Result',
	'Texts' : 'Texts_Result',
	'Images' : 'Images_Result',
	'Has' : 'Has_Result'
}

#parses recieved response into json
def load_data(response):
	#response = requests.get('http://templex.cnetcontent.com/Tps')
	soup = BeautifulSoup(response.text,'html.parser')
	result = soup.find_all('textarea')
	return result

#gets a specified part of loaded data
def get_data(data,part):
	for area in data:
		if area.attrs['id'] == part:
			partData = json.loads(area.text)
	return partData

#returns ready-to-use string for POST request
def set_parameters(lang, market, sku):
	parameters = lang + market + 'skuId=' + sku
	return {'Parameters' : parameters}

def create_html_table(data):
	
	#html = '<table><tr><th>' + '</th><th>'.join(data.keys()) + '</th></tr>'
	#for row in zip(data.values()):
	for key in data:
	    newDict = {}
	    keys = []
	    valsSeparated = {}
	    print(type(key))
	    #print('key:',key, 'data[key]:',data[key], 'type:', type(data[key]))
	    if isinstance(key,int) or isinstance(key,str):
	    	#print('bloaoaoaooa',type(data[key]))
	    	newDict[key] = key
	    elif isinstance(key,dict):
	    	#print('ohohohoh', type(data[key]))
	    	for key2 in key:
	    		create_html_table(key2)
	    	#vals = data[key][0]
	    	#print(vals[0])
	    	#for key2 in vals:
	    	#	print(key2,vals[key2])
	    #html += '<tr><td>' + '</td><td>'.join(str(row)) + '</td></tr>'
	#pprint(valsSeparated)
	#html += '</table>'

	return newDict

#initializing data from configuration file
config = configparser.ConfigParser()
config.readfp(open('settings.ini'))
lang = 'lang=' + config.get('parameters','lang') + '&' #loading language
market = 'market=' + config.get('parameters','market') + '&' #loading market
path = config.get('paths','requestPath')
skuList = preproc_skus(path)[:1] #loading sku list

for sku in skuList:
	#setting POST request parameters
	url = 'http://templex.cnetcontent.com/Tps'
	headers = {'Content-Type': 'application/x-www-form-urlencoded'}
	data = set_parameters(lang, market, sku)

	#sending POST request
	response = requests.post(url=url, headers=headers, data=data)
	if response.status_code == 200:
		data = load_data(response)
		altCatsData = get_data(data,dataIDs['AltCats']) #loading alternative category data
		altCatsDataSet = altCatsData['data']['tx-alt-cats']['categories'] #only caterories
		avusData = get_data(data,dataIDs['Avus'])
		avusDataSet = avusData['data']['tx-avus']['avus']
		for category in altCatsDataSet:
			if category['version'] == int(config.get('parameters','alternativeCategoryVersion')):
				print(str(category['version'])+':',category['key'],category['name'])
		print(create_html_table(avusDataSet))