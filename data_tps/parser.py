import requests
from bs4 import BeautifulSoup
from websocket import WebSocket
import logging
import datetime
import json
from urllib.parse import urlencode
import webbrowser

logging.basicConfig(filename='parser.html', level=logging.DEBUG, filemode='w')

url = 'http://templex.cnetcontent.com/Account/Login'
params = {'ReturnUrl': '/Home/Parser'}
headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6',
    'Connection': 'keep-alive',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
}

with requests.Session() as s:
    soup = BeautifulSoup(s.get(url).text, 'html.parser')
    token_data = soup.find('input', {'name': '__RequestVerificationToken'})['value']
    payload = {
        'Email': 'sera@ciklum.com',
        'Password': 'alexalex',
        '__RequestVerificationToken': token_data,
        'RememberMe': 'False'
    }
    response = s.post(
        url,
        params=params,
        headers=headers,
        data=payload,
        allow_redirects=True
    )

    print(str(requests.utils.dict_from_cookiejar(s.cookies)))
    logging.debug(response.text)

    url = 'http://templex.cnetcontent.com/signalr/negotiate'
    params = {
        'clientProtocol': '1.5',
        'connectionData': '[{"name":"parserhub"}]',
        '_': datetime.datetime.timestamp(datetime.datetime.utcnow()),
    }
    response = s.get(url, params=params)
    conn_token = json.loads(response.text)['ConnectionToken']

    url = 'http://templex.cnetcontent.com/signalr/start'
    params = {
        'transport': 'webSockets',
        'clientProtocol': '1.5',
        'connectionToken': conn_token,
        'connectionData': '[{"name":"parserhub"}]',
        '_': datetime.datetime.timestamp(datetime.datetime.utcnow())
    }

    resp = s.get(url, params=params)
    print(resp.text)

    dummy_params = {
        'transport': 'webSockets',
        'clientProtocol': '1.5',
        'connectionToken': conn_token,
        'connectionData': '[{"name":"parserhub"}]',
        'tid': '6'
    }
    url = 'ws://templex.cnetcontent.com/signalr/connect?{}'.format(urlencode(dummy_params))
    cookies = urlencode(requests.utils.dict_from_cookiejar(s.cookies))

    ws = WebSocket()
    ws_headers = {
        'Host': 'templex.cnetcontent.com',
        'Connection': 'Upgrade',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'Upgrade': 'websocket',
        'Origin': 'http://templex.cnetcontent.com',
        'Sec-WebSocket-Version': '13',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6',
        'Cookie': cookies,
        'Sec-WebSocket-Extensions': 'permessage-deflate; client_max_window_bits'
    }
    ws.connect(
        url,
        header=ws_headers,
        cookie=cookies,
        host='templex.cnetcontent.com',
        origin='http://templex.cnetcontent.com'
    )

    result = ws.recv()
    webbrowser.open('parser.html')
    print(result)