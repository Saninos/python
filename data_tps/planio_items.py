import requests
from bs4 import BeautifulSoup
from pprint import pprint
from openpyxl import load_workbook
from html.parser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(str(html))
    return s.get_data()

LOAD_FROM_EXCEL = False

if LOAD_FROM_EXCEL:
    wb = load_workbook('item_links.xlsx')
    ws = wb.active

    links = []

    for col in ws.iter_cols(min_row=2, max_col=1, max_row=300):
        for cell in col:
            links.append(cell.value)
else:
    links_excel = '''https://claims.cnetcontent.com/issues/52289
https://claims.cnetcontent.com/issues/52293
https://claims.cnetcontent.com/issues/53052
https://claims.cnetcontent.com/issues/53143
https://claims.cnetcontent.com/issues/53339
https://claims.cnetcontent.com/issues/53353
https://claims.cnetcontent.com/issues/53361
https://claims.cnetcontent.com/issues/61946
https://claims.cnetcontent.com/issues/61960
https://claims.cnetcontent.com/issues/61968
'''
    links = links_excel.splitlines()

for link in links:
    if link is not None or link != '':
        response = requests.get(link, auth=('aserebrov', 'yG38wFi2'))
        soup = BeautifulSoup(response.text, 'html.parser')
        result = soup.find('div', {'class': "subject"})
        item_name_pre = strip_tags("".join(str(result).split('</div>')).split('<div>')[-2:])
        item_name = item_name_pre.strip('[]').split(', ')
        temp_list = []
        for item in item_name:
            item1 = item.strip("''")
            if item1.find(':') != -1:
                temp_list.append(item1[item1.find(':') + 2:])
            elif item1.find('\\') != -1:
                temp_list.append(item1[:item1.find('\\')])
        print('%s' % '\t'.join(map(str, temp_list)))
