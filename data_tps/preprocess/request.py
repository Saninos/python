from openpyxl import Workbook, load_workbook
import configparser, os

idcounter = 0

def get_sku_ID_column(worksheet):
	row1 = list(worksheet.rows)[0]
	IDCell = ''
	idcounter = 0
	for cell in row1:
		if str(cell.value) in ('TX_SKU_ID','SystemID'):
			IDCell = cell
			idcounter += 1
	return [IDCell, idcounter]

def get_sku_list(worksheet, SKUcolumn):
	skuList = []
	for cell in worksheet[SKUcolumn.column]:
		if cell.value is not None: 
			if cell.value.find('s'): 
				skuList.append(cell.value[1:]) #adds sku to list, without starting S
			else:
				skuList.append(cell.value)
	return skuList[1:] #skips column title

def main(path):

	wb = load_workbook(path)
	ws = wb.active

	skuIDCell = get_sku_ID_column(ws)[0]
	idcounter = get_sku_ID_column(ws)[1]

	#column with system ID found, and it's single
	if idcounter == 1:
	#	print(skuIDCell.column)
		SKUs = get_sku_list(ws,skuIDCell)
		return SKUs

	#column with system ID is not found, defaults to the first column
	elif idcounter == 0:
		skuIDCell = list(ws.columns)[0][0]
		SKUs = get_sku_list(ws,skuIDCell)
		return SKUs
		
	#multiple columns with system ID found, returning an error
	else:
		print('Error: Multiple system ID columns found. File processing interrupted.')
		SKUs = []
		return SKUs

if  __name__ == '__main__':
	config = configparser.ConfigParser()
	config.readfp(open('\\'.join((os.getcwd().split('\\')[:-1]))+'\\settings.ini'))
	path = config.get('paths','requestPath')
	main(path)