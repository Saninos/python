def process_skus(skuInitList):

	skuPreprocList = skuInitList.splitlines()
	skuProcList = []

	#debug print
	#print(skuPreprocList[1],skuPreprocList[1][1:],len(skuPreprocList[1]),skuPreprocList[1].find('S'))

	for sku in skuPreprocList:
		if len(sku) > 0:
			if sku.find('s') or sku.find('S'):
				skuProcList.append(sku[1:])
			else:
				skuProcList.append(sku)

	return skuProcList