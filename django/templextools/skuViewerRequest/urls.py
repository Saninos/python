from django.conf.utils import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
]