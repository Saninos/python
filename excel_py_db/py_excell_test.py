from pymongo import MongoClient
from openpyxl import load_workbook
from pprint import pprint

client = MongoClient()

db = client['test-database']
altCats = db.altCats

wb = load_workbook(filename = 'Templex-Attribute_Reference_v14.xlsx')
ws = wb.active
altCats_dict = {}

for row in ws.iter_rows(min_row=3, min_col = 1, max_col=4, max_row=10):
	altCats_dict[str(row[0].value)] = {
		'name' : str(row[1].value),
		'legacy_category' : str(row[2].value),
		'path' : str(row[3].value)
	}

altCats.insert(altCats_dict)
pprint(altCats)
