import os, csv, fnmatch
from openpyxl import load_workbook, Workbook
from pprint import pprint

dataPath = os.path.join('\\'.join(os.path.realpath(__file__).split('\\')[:-1]),'data')
EXPORT_REPORT = 'TEMPLEX_EXPORT_REPORT.xlsx'

exportReportFile = os.path.join(dataPath, EXPORT_REPORT)
wb = load_workbook(exportReportFile)
ws = wb.active

reportDict = {}
oneColList = []

for i in range(1,ws.max_column):
	for column in ws.iter_rows(min_col=i, max_col=i):
		for cell in column:
			oneColList.append(cell.value)
	reportDict[oneColList[0]] = oneColList[1:]

print(reportDict.keys())