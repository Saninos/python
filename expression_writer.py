attributes = """A00354
A00355
A00356
A00357
"""
"""
A06592
A06584
A06582
A06586
A06585
A06587
A06588
A06590
A06591
A06593
A06707
A06704
A06705
A06627"""

for i, attr in enumerate(attributes.splitlines()):
	attr_id = attr[2:]
	# A[xxx].Value.IfLike("Yes", "")
	formatter = 'A[{}].{}'
	attr_name = formatter.format(attr_id, 'Name')
	attr_value = formatter.format(attr_id, 'Value')
	attr_values = formatter.format(attr_id, 'Values.Flatten()')
	print('{}_" "_{}_": "_{};'.format(i, attr_name, attr_value))
	# print('{}_" "_{}_": "_{};'.format(i, attr_name, attr_values)