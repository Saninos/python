import re
from collections import OrderedDict
from pprint import pprint


categories = """
A Systems
AA Desktops & Servers
AB Notebooks
AC Tablets & e-Book Readers
AD Processor Upgrades
AE Motherboards
AF System Cabinets
AG UPS & Power Devices
AH System & Power Cables
AI Port Expansion
AJ System Accessories
AK System Service & Support

B Memory

BA RAM
BD Flash Memory
BE Read-Only Memory
BF Memory Adapters

C Storage Devices
CA Hard Drives
CC Disk Drives
CD Tape Drives
CE Network Storage
CF Removable Media
CG Storage Adapters
CH Storage Cables
CI Storage Enclosures & RAID Arrays
CJ Storage Accessories
CK Storage Service & Support

D Input Devices

DA Keyboards
DB Pointing Devices
DC Game Controllers
DD Scanners
DE Bar Code Readers
DG PC & Network Cameras
DH Input Adapters
DI Input Cables
DJ Input Accessories
DK Input Service & Support

E Output Devices
EA Monitors
EC Projectors
ED Printers
EF Video Cards
EG Sound Cards
EH Output Cables
EI Printer Consumables
EJ Output Accessories
EK Output Service & Support

F Networking

FA Modems
FB Video Conferencing
FC Concentrators & Multiplexers
FD Repeaters & Transceivers
FE Hubs & Switches
FF Bridges & Routers
FG Network Devices
FH Network Adapters
FI Network Cables
FJ Network Accessories
FK Network Service & Support
FL Network Testers
FM Business Phone Systems

G Software
GA Operating Systems
GB Applications
GC Software Service & Support​

H Learning Resources

HA Reference Material
HB Training Courses

O Office Machines

OA Multifunction & Office Machines
OB Office Shredders
OC Binding Machines
OD Typewriters
OE Calculators
OF Cash Registers
OG Time & Attendance Control

Y Area Security Systems

YA Time Lapse VCRs & DVRs
YB Quad Processors & Video Multiplexers
YC Video Surveillance Kits & Solutions


J Video

JA Televisions & Flat Panels
JB VCRs
JC Blu-ray & DVD Players
JD Camcorders & Digital Cameras
JE Web TV
JF Video Game Consoles
JG Satellite TV
JH DVRs
JI Combined AV Devices
JJ Car Audio / Video
JK Digital AV Players & Recorders

K Audio

KA Home Audio
KB Portable Audio
KC Car Audio
KD Speakers
KE Headsets & Microphones

L Cameras & Optical Systems

LA Cameras
LB Lenses
LC Flashes
LD Binoculars
LE Telescopes
LF Filters
LG Film
LH Microscopes

M CE Options

MA Antennas
MB Tripods & Video Support Equipment
MC AV Blank Media
MD AV Furniture
ME Carrying Cases
MF Remote Controls
MG Power Devices & Batteries
MH Cables
MI Accessories
MJ Service & Support
MK Photo & Video Accessories
MM Gift & Prepaid Cards

N Specialized Electronics

NA Radar Detectors
NB Car Security Systems

P Communication

PA Telephones
PB Mobile Phones
PC Pagers
PD Answering Machines & Caller ID
PE Two-Way Radios
PF GPS Receivers
PG GPS Kits
PH GPS Software
PI Marine Electronics
PJ Wearable Electronics

T Studio Equipment

TA Backgrounds
TB Lighting Filters
TC Light Control
TD Studio Light & Flashes
TE Studio Mounting
TG Light Meters
U Darkroom

UA Photo Enlarging Paper
UB Photographic Chemistry
UD Enlargers & Components
UE Developing & Processing Equipment
UF Darkroom Accessories, Safelight & Setup

V Photo Storage & Presentation

VA Photo Frames
VB Photo Albums & Archival Storage
VC Cutters & Trimmers
VD Laminators

W Professional Video 

WA Video Editing Controllers, Mixers & Titlers
WB Studio & Field Monitors
WC Studio & Production Equipment

X Professional Audio

XA Audio Mixers


R Large Appliances

RA Refrigerators & Freezers
RB Ranges & Ovens
RC Cooktops
RD Hoods
RE Dishwashers
RF Microwave Ovens
RG Large Appliance Accessories
RH Washers & Dryers

 

S Small Appliances

SA Vacuum Cleaners
SB Food Processors
SC Coffee Makers
SD Irons
SE Toasters & Grills
SF Personal Care
SG Heaters & Coolers
SH Small Kitchen Appliances
SI Small Appliances Accessories
SJ Tableware
SK Cutlery & Utensils
SL Food Storage
SM Cookware & Bakeware
SN Weather Instruments 
SO Sewing Machines


0 Office Supplies

0A Books & Pads
0B Forms
0C Writing & Drawing Tools
0D Craft & Art Supplies
0E Filing & Storage
0F Presentation
0G Presentation Accessories
0H Clips, Pins & Staples
0I Stamps & Accessories
0J Desk Accessories

1 Shipping & Packaging

1A Envelopes & Shipping Boxes
1B Tape & Dispensers
1C Packaging Materials
1D Mailroom & Packaging Equipment

2 Janitorial & Facilities

2A Trash Bags & Cans
2B Cleaning Tools
2C Household Chemicals
2D Hygiene
2E Lighting
2I Workwear & Safety
2J Signage & Posters

3 Food & Beverages

3A Food
3B Beverages

4 Furniture

4A Surfaces
4B Seating
4C Storage Furniture
4D Decoration
4K Furniture Accessories

 

5 Construction & Repair

5A Hand Tools
5B Power Tools
5C Construction Equipment
5D Locking Systems
5E Integrated Systems & Automation 
5F Outdoor Power Tools 
5G Power Tool Accessories

9 Clothing, Shoes & Jewelry

9C Watches


6 Toys, Kids & Baby

6A Toys
6B Blocks & Building Sets
6C Games & Puzzles
6D Outdoor Play
"""

category_code_regex = re.compile(r'^\w\w')
class_code_regex = re.compile(r'^\w')
ampersand_regex = re.compile(r'&')

categories_dict = OrderedDict()
class_dict = OrderedDict()

for line in categories.splitlines():
    if line is not "" and line is not None:
        line_begin = line.split(' ')[0]
        line_end = ' '.join(line.split(' ')[1:])
        if re.search(category_code_regex, line_begin) is not None:
            categories_dict[line_begin] = line_end
        elif re.search(class_code_regex, line_begin):
            class_dict[line_begin] = line_end

"""
CASE SKU.CategoryCode
    WHEN "{}" THEN "{}"
"""

print('CASE SKU.CategoryCode')
for cat_code, cat_name in categories_dict.items():
    if re.search(ampersand_regex, cat_name) is None:
        print('\tWHEN "{}" THEN "{}"'.format(cat_code, cat_name.lower()))
    elif re.search(ampersand_regex, class_dict[cat_code[0]]) is None:
        print('\tWHEN "{}" THEN "{}"'.format(cat_code, class_dict[cat_code[0]].lower()))

