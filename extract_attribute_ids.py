import re
expression = '''IF A[5033].Invariant.Erase("none") IS NOT NULL
OR A[2679].Values IS NOT NULL 
OR A[3824].Where("%duplex%").Values IS NOT NULL 
OR A[2262].Where("%duplex%").Values IS NOT NULL 
OR A[202].Where("%duplex%").Values IS NOT NULL 
THEN "Sí";
'''
attrs_list = []
for attribute in map(int, re.findall(r'\d+', expression)):
	attrs_list.append(attribute)
i = 1
for attribute in attrs_list:
	valueExp = str(i) + '_A[' + str(attribute) + '].Value;'
	valuesExp = str(i) + '_A[' + str(attribute) + '].Values.Flatten();'
	print(valueExp,'\n',valuesExp)
	i += 1