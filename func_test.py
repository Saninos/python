def is_cool(number): # limited by 0 <= number <= 99
    return number < 10 or (int(number) / 10 == number % 10)

n = 100
for x in range(n):
    print x, is_cool(x)