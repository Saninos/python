https://cnet.plan.io/issues/116776  "Package Weight": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116775  "Package Width": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116774  "Package Height": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116773  "Package Length": Defaults to "0.02" when missing
https://cnet.plan.io/issues/106345  "What's in the Box": Defaults to product name when missing
https://cnet.plan.io/issues/106339  "Is the product regulated as hazardous material or dangerous goods/ substances (also called 'Hazmat')?": Always "No"
https://cnet.plan.io/issues/106335  "Lithium Battery Energy Content (in Watt-Hours)": Missing values here are NOT considered partial exports
https://cnet.plan.io/issues/106333  "How is the Lithium Battery packaged?": Defaults to "batteries_packed_with_equipment" when missing for products with lithium battery
https://cnet.plan.io/issues/106331  "Battery Weight (in grams)": This information is not generally available from public sources ; Missing values here are NOT considered partial exports
https://cnet.plan.io/issues/106329  "Number of Batteries included": Defaults to "1" when missing for products with values "rechargeable", "power bank", "lithium battery", Chargers with battery, Headphones and Speakers with battery,  and Wireless Keyboards with battery
https://cnet.plan.io/issues/106323  "Are batteries included with the product?": Always "No" for Cables
https://cnet.plan.io/issues/106321  "Are batteries needed to power the product or is this product a battery?": Always "No" for Cables
https://cnet.plan.io/issues/106309  "Is the product a Consumer Commodity ORM-D?": Always "No"
https://cnet.plan.io/issues/106305  "Does your product require a cautionary statement because it is one of the following:a. Toy or a game intended for use by children who are at least 3 years of age but not older than 6 years old and contains a small part;b. Any latex balloon;c. Any ba...": Always "No"
https://cnet.plan.io/issues/106303  "Battery Life (in Hours)": We export Run Time
https://cnet.plan.io/issues/106297  "Country of Origin": Defaults to "China" when missing
https://cnet.plan.io/issues/106289  "Product weight in pounds": Defaults to Package Weight when missing
https://cnet.plan.io/issues/106287  "Item Width (in inches)": Defaults to Package Width when missing; Defaults to "0.02" when missing for cables
https://cnet.plan.io/issues/106285  "Item Height (in inches)": Defaults to Package Height when missing; Defaults to "0.02" when missing for cables
https://cnet.plan.io/issues/106283  "Item Length (in inches)": Defaults to Package length when missing
https://cnet.plan.io/issues/106281  "Screen Size": Defaults to "0.1" when required and missing
https://cnet.plan.io/issues/106277  "Does this have a screen?": Always "No" for wireless cell phone keyboards, cellular phone cables, bluetooth headphones
https://cnet.plan.io/issues/106263  "Color name/Pattern Name": Defaults to "Unspecified" when missing
https://cnet.plan.io/issues/106257  "Is Variation Item": Defaults to "No" when missing
https://cnet.plan.io/issues/106249  "Compatible Phone Models": Defaults to "Universal/SmartPhones" when missing for keyboards with values "bluetooth", "android", and "ios"; Defaults to "Unspecified" when a value is required and missing for phone accessories
https://cnet.plan.io/issues/106239  "Headset Type": Defaults to "Two-ear" when missing for Stereo headsets
https://cnet.plan.io/issues/106233  "Is a ‘replacement part'?": Always "No"
https://cnet.plan.io/issues/106231  "Product Launch Date": Defaults to date of delivery when missing
https://cnet.plan.io/issues/106193  "Vendor Code": Defaults to "Ingram Micro CE, us consumer electronics, INGB9" when missing