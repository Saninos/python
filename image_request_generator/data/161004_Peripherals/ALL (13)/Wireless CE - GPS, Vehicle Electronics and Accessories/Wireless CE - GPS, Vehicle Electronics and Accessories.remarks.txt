https://cnet.plan.io/issues/116757  "Package Weight": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116754  "Package Width": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116744  "Package Height": Defaults to "0.02" when missing
https://cnet.plan.io/issues/116738  "Package Length": Defaults to "0.02" when missing
https://cnet.plan.io/issues/109489  "What's in the Box": Defaults to product name when missing
https://cnet.plan.io/issues/109477  "Battery Weight (in grams)": This information is not generally available from public sources ; Missing values here are NOT considered partial exports
https://cnet.plan.io/issues/109461  "Product weight in pounds": Defaults to Package Weight when missing
https://cnet.plan.io/issues/109459  "Is the product regulated as hazardous material or dangerous goods/ substances (also called 'Hazmat')?": Always "No"
https://cnet.plan.io/issues/109455  "Are batteries needed to power the product or is this product a battery?": Always "No" for Cables, Cabling Accessories, Antennas, Mounts, Software, Camcorder Accessories
https://cnet.plan.io/issues/109445  "Is a ‘replacement part'?": Always "No"
https://cnet.plan.io/issues/109425  "Does your product require a cautionary statement because it is one of the following:a. Toy or a game intended for use by children who are at least 3 years of age but not older than 6 years old and contains a small part;b. Any latex balloon;c. Any ba...": Always "No"
https://cnet.plan.io/issues/109415  "Screen Size": Defaults to "0.1" when a value is required and missing
https://cnet.plan.io/issues/109383  "Total Harmonic Distortion at Rated RMS Power": We export "Distortion Factor" for Amplifiers when value is required and missing
https://cnet.plan.io/issues/109367  "Faceplate Type": Defaults to "Unspecified" when a value is required and missing
https://cnet.plan.io/issues/109365  "Number of Equalizer Band Channels": Defaults to "1" when missing for products with values "Equalizer"
https://cnet.plan.io/issues/109352  "RMS Power Range - Speakers": We export maximum power; Defaults to "0" when a value is required and missing
https://cnet.plan.io/issues/109350  "Sensitivity (in db)": Defaults to "Unspecified" when a value is required and missing
https://cnet.plan.io/issues/109348  "Maximum Speaker Depth (in inches)": Defaults to "0.1" when a value is required and missing
https://cnet.plan.io/issues/109346  "Speaker Cutout Diameter or Length (in inches)": Defaults to "0.1" when a value is required and missing
https://cnet.plan.io/issues/109344  "Speaker Composition": Defaults to "Unspecified" when a value is required and missing; Always "Unspecified" for Subwoofer Drivers
https://cnet.plan.io/issues/109342  "Speaker Size (in Inches)": We export "Driver Diameter" when required and missing; We export "Driver Diameter" for Subwoofer driver
https://cnet.plan.io/issues/109340  "Design of speaker": Defaults to "Unspecified" when a value is required and missing; Always "Unspecified" for Subwoofer Drivers
https://cnet.plan.io/issues/109338  "Number of Speakers": We export driver quantity for Subwoofer Drivers
https://cnet.plan.io/issues/109308  "Memory Card Slot": We export "Memory card slot" information; Defaults to "Unspecified" when a value is required and missing
https://cnet.plan.io/issues/109300  "Display Type": Defaults to "Unspecified" when a value is required and missing
https://cnet.plan.io/issues/109298  "Display Size (in inches)": Defaults to "0.1" when a value is required and missing
https://cnet.plan.io/issues/109296  "Does this have a screen?": Always "No" for Cables and Mounts
https://cnet.plan.io/issues/109290  "Item Width (in inches)": Defaults to Package Width when missing
https://cnet.plan.io/issues/109288  "Item Height (in inches)": Defaults to Package Height when missing
https://cnet.plan.io/issues/109286  "Item Length (in inches)": Defaults to Package Length when missing
https://cnet.plan.io/issues/109276  "Product Launch Date": Defaults to date of delivery when missing
https://cnet.plan.io/issues/109274  "Country of Origin": Defaults to "China" when missing
https://cnet.plan.io/issues/109222  "Is Variation Item": Defaults to "No" when missing
https://cnet.plan.io/issues/109216  "Product Classification": 3-way and 4-way car speakers are classified as "Car Speakers - Coaxials" for lack of more suitable category
https://cnet.plan.io/issues/109210  "Vendor Code": Defaults to "Ingram Micro CE, us consumer electronics, INGB9" when missing