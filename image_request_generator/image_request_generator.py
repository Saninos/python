import os
import fnmatch
from openpyxl import load_workbook, Workbook
from pprint import pprint

def get_sku_ID_column(worksheet):
	row1 = list(worksheet.rows)[4]
	IDCell = ''
	for cell in row1:
		if fnmatch.fnmatch(str(cell.value),'Vendor SKU*'):
			IDCell = cell
			print(IDCell.column)
			break
	return IDCell

def get_sku_list(worksheet, SKUcolumn):
	skuList = []
	#import pdb; pdb.set_trace()
	skuDict = {}
	for i, cell in enumerate(worksheet[SKUcolumn.column][6:]):
		if cell.value is not None:
			print(cell, cell.value)
			skuList.append(cell.value)
			skuDict[cell] = cell.value
	pprint(skuDict)
	return skuList

dirsList = []
imagesSkus = []
errorFiles = []

dataPath = os.path.join('\\'.join(os.path.realpath(__file__).split('\\')[:-1]),'data')


for (dirpath, dirnames, filenames) in os.walk(os.path.dirname(dataPath)):
	if fnmatch.fnmatch(dirpath,'*data\\*'): dirsList.append(dirpath)
try:
	#parentDir = ''.join(dirsList[1].split('\\')[-1:])
	del(dirsList[0])
except IndexError:
	print("No export folders found")
	exit()

for directory in dirsList:
	for (dirpath, dirnames, filenames) in os.walk(directory):
		for filename in filenames:
			if fnmatch.fnmatch(filename.lower(), '*.xlsm') and not fnmatch.fnmatch(filename.lower(),'~$*'):
				print('Processing',filename)
				try:
					wb = load_workbook(os.path.join(dirpath,filename))
					ws = wb.active

					wb_result = Workbook()
					ws_result = wb_result.active

					skuCell = get_sku_ID_column(ws)
					skuList = get_sku_list(ws, skuCell)

					ws_result.cell(row=1,column=1).value = 'TX_SKU_ID'

					for i, sku in enumerate(skuList):
						imagesSkus.append(sku)
						ws_result.cell(row=i+2,column=1).value = sku

					filenameProcessed = ''.join([i for i in filename.split('.') if not fnmatch.fnmatch(i,'xlsm')])
					separateSavePath = os.path.join(dataPath,filenameProcessed+'_images_request.xlsx')
					wb_result.save(separateSavePath)
					print('Found and saved',len(skuList),'SKUs')

				except FileNotFoundError:
					errorFiles.append(filename)
					pass

print('In total there are',len(imagesSkus),'SKUs found and saved to data\\images_request.xslx file')

wb = Workbook()
ws = wb.active

ws.cell(row=1,column=1).value = 'TX_SKU_ID'

for i, sku in enumerate(imagesSkus):
	ws.cell(row=i+2,column=1).value = sku

savePath = os.path.join(dataPath,'_all_images_request.xlsx')
wb.save(savePath)

if len(errorFiles) > 0: #if there are error files
	print('\nWARNING!\nThe following files are excluded:')
	for errorFile in errorFiles:
		print(errorFile)