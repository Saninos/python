from zipfile import ZipFile
import zipfile
import webbrowser
from PIL import Image
import os
import re
import argparse
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger()


def find_images(zip_archive):
    images_filenames = []
    jpeg_regex = re.compile(r'.*\.jpg')
    for infobit in zip_archive.infolist():
        if re.search(jpeg_regex, infobit.filename) is not None:
            images_filenames.append(infobit.filename)
    return images_filenames


imgs_bad = {}                       # blank dictionary for small images
open_html = True
images = []
html = '<body bgcolor=#D3D3D3>'
htmlParagraph = '<p align=center>'
htmlParagraphEnd = '</p>'
htmlBr = '<br>'
htmlUrlBegin = '<a href='


parser = argparse.ArgumentParser(
    description='Analyse provided zip archive with images'
)
parser.add_argument(
    '-a',
    '--archive',
    action='store',
    dest='filename',
    default=None,
    help='Required archive filename containing images',
    required=True
)

parser.add_argument(
    '-s',
    '--size',
    action='store',
    dest='allowed_size',
    default=500,
    help='Minimal allowed image size, as a side of a square, e.g. 300 for 300x300 px',
    required=False
)

results = parser.parse_args()
filename = results.filename
allowed_size = (int(results.allowed_size), int(results.allowed_size))

if not zipfile.is_zipfile(filename):
    print('Please provide a valide archive name')
    exit()

with ZipFile(filename, 'r') as myzip:
    images = find_images(myzip)
    images_len = len(images)
    logger.info('Starting analysis of {} images'.format(images_len))
    for i, image in enumerate(images):
        with myzip.open(image) as filename:
            with Image.open(filename) as im:
                logging.debug(
                    '{} {} \t {} x {} px'
                    .format(
                        i + 1,
                        image,
                        im.size[0],
                        im.size[1]
                    )
                )
                if im.size[0] < allowed_size[0] or im.size[1] < allowed_size[1]:
                    # webbrowser.open(urls[i])
                    imgs_bad[image] = im.size
    myzip.close()

# prints url - image_size
if len(imgs_bad) > 0:
    logging.info('Small images:')
    for key, size in imgs_bad.items():
        logging.info('{} - {}x{} px'.format(key, size[0], size[1]))
        badImageUrl = "".join([s.lower() for s in str(key)])
        htmlBegin = '{}{}>{}</a> - {}x{} px'.format(
            htmlUrlBegin,
            badImageUrl,
            badImageUrl,
            size[0],
            size[1]
        )
        htmlImg = '<img src={}>'.format(key)
        html += htmlParagraph + htmlBegin + htmlBr + htmlImg
else:
    logging.info('All images are bigger than {}x{} px'.format(allowed_size[0], allowed_size[1]))
    open_html = False

if open_html:
    with open('bad_images.html', 'w') as f:
        f.write(html)
    path = os.path.dirname(os.path.realpath(__file__))
    webbrowser.open('file://{}/bad_images.html'.format(path))
