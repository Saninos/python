"""Image analysis.

Takes a list of image urls, returns images less
than 500x500 on the separate webpage

"""

import webbrowser
import os
import requests
from fnmatch import fnmatch
from PIL import Image
import re

html = '<body bgcolor=#D3D3D3>'
htmlParagraph = '<p align=center>'
htmlParagraphEnd = '</p>'
htmlBr = '<br>'
htmlUrlBegin = '<a href='

# copy-paste here links from MAIN IMAGE column, as is
urls_excel = """http://cdn.cnetcontent.com/38/b2/38b21db3-23c8-4537-99d0-816e1a650b1c.jpg
http://cdn.cnetcontent.com/af/f4/aff40114-bfe9-4cd2-9be1-2c340e01ef39.jpg
http://cdn.cnetcontent.com/16/c1/16c17195-3a8c-4621-bd70-6ffcf8f18060.jpg
http://cdn.cnetcontent.com/f4/3a/f43a4ea8-278f-4fa2-b21c-34eee58fdf55.jpg
http://cdn.cnetcontent.com/0b/05/0b05bef7-97af-4468-9149-cb7a84086259.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/1c/19/1c19fbc1-20bb-4abc-a5f1-aad701d0c2ca.jpg
http://cdn.cnetcontent.com/00/fe/00feaee3-489e-47db-bb39-8eca75b74203.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/54/0c/540c9fb3-306c-4b96-92ef-79b1ae314f3a.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/77/24/7724ebf8-a28c-4e58-be52-ab85529bf376.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/fd/b4/fdb4f66d-2364-4240-acbd-9d3255a25f87.jpg
http://cdn.cnetcontent.com/6c/91/6c91fe0e-a14f-43e9-a428-ae9095946e67.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/bf/6c/bf6c54fe-d946-42f0-834d-2821f27c37ba.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/17/1f/171fbb28-bffc-4481-805e-0f85ae1e748e.jpg
http://cdn.cnetcontent.com/17/1f/171fbb28-bffc-4481-805e-0f85ae1e748e.jpg
http://cdn.cnetcontent.com/51/2e/512e82e7-971d-4b4e-af23-b8e7f2bcd408.jpg
http://cdn.cnetcontent.com/f4/c2/f4c22cc4-f834-41aa-ab81-61fe9ed2fbe7.jpg
http://cdn.cnetcontent.com/2b/ed/2bed4fe6-5f64-41a6-89db-4a5f91a9812f.jpg
http://cdn.cnetcontent.com/f4/c2/f4c22cc4-f834-41aa-ab81-61fe9ed2fbe7.jpg
http://cdn.cnetcontent.com/2b/ed/2bed4fe6-5f64-41a6-89db-4a5f91a9812f.jpg
http://cdn.cnetcontent.com/7d/f0/7df07083-04e4-42d2-b1d1-cdc91e5a7fb1.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/01/91/0191d120-68c8-4a2f-8f45-0d3405da41e1.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/6a/7e/6a7e12b1-dee9-412f-af20-2b55a37c22f9.jpg
http://cdn.cnetcontent.com/85/7f/857f0879-794c-4749-a623-8dd3e1fab412.jpg
http://cdn.cnetcontent.com/32/53/3253f237-d575-401b-8d46-c9f9cdb1b5d9.jpg
http://cdn.cnetcontent.com/77/b5/77b5902f-8569-4f1b-a963-dd0b46a8eab8.jpg
http://cdn.cnetcontent.com/32/53/3253f237-d575-401b-8d46-c9f9cdb1b5d9.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/cd/59/cd59acda-5034-4cbb-8069-ee1ce2f6d31b.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/8c/6e/8c6e3771-9f2e-443f-8673-e3e54552061c.jpg
http://cdn.cnetcontent.com/40/78/40786ba3-f020-4fad-8d5f-dd4563c7f335.jpg
http://cdn.cnetcontent.com/8c/6e/8c6e3771-9f2e-443f-8673-e3e54552061c.jpg
http://cdn.cnetcontent.com/c7/d4/c7d48af0-a9c6-491a-a13a-f6cc3abd83b4.jpg
http://cdn.cnetcontent.com/fb/b6/fbb61063-5cbf-4405-a2d4-8380fdbc547e.jpg
http://cdn.cnetcontent.com/fb/b6/fbb61063-5cbf-4405-a2d4-8380fdbc547e.jpg
http://cdn.cnetcontent.com/c7/d4/c7d48af0-a9c6-491a-a13a-f6cc3abd83b4.jpg
http://cdn.cnetcontent.com/77/bc/77bc63cc-9512-4724-8635-dd76b8ce9b71.jpg
http://cdn.cnetcontent.com/fb/b6/fbb61063-5cbf-4405-a2d4-8380fdbc547e.jpg
http://cdn.cnetcontent.com/48/ef/48efa25c-4a25-4738-97d1-9abac5234069.jpg
http://cdn.cnetcontent.com/aa/29/aa298138-eb66-48ec-9922-942594116610.jpg
http://cdn.cnetcontent.com/94/ac/94ac6be4-9ddf-49a0-a137-ada860d3dc70.jpg
http://cdn.cnetcontent.com/23/03/230318bc-d1bf-4c70-bd0b-98c1104e6428.jpg
http://cdn.cnetcontent.com/ef/3f/ef3fb39c-d24a-44bc-b204-40c778a16b1d.jpg
http://cdn.cnetcontent.com/9b/d3/9bd32668-4dc3-4294-a23a-041c32f9a049.jpg
http://cdn.cnetcontent.com/0d/13/0d1305d9-a1bb-4a88-81f2-c46e0ac11135.jpg
http://cdn.cnetcontent.com/ef/29/ef296294-14bd-4b56-8ce5-1843560d4e64.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/97/16/97161684-9ea2-4cb7-9d39-9fe4507720de.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/51/2e/512e82e7-971d-4b4e-af23-b8e7f2bcd408.jpg
http://cdn.cnetcontent.com/0d/13/0d1305d9-a1bb-4a88-81f2-c46e0ac11135.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/82/6b/826bc0a2-7b97-492a-8096-6e539e404c58.jpg
http://cdn.cnetcontent.com/17/1f/171fbb28-bffc-4481-805e-0f85ae1e748e.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/d1/e0/d1e05d0e-dab5-4ff8-a594-b6dd3ef8ac5c.jpg
http://cdn.cnetcontent.com/17/1f/171fbb28-bffc-4481-805e-0f85ae1e748e.jpg
http://cdn.cnetcontent.com/51/c6/51c6a95e-997a-4cbe-b880-2b690a578726.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/51/c6/51c6a95e-997a-4cbe-b880-2b690a578726.jpg
http://cdn.cnetcontent.com/51/c6/51c6a95e-997a-4cbe-b880-2b690a578726.jpg
http://cdn.cnetcontent.com/45/6b/456b470f-f197-4ebb-8bb7-9a4fde1cb094.jpg
http://cdn.cnetcontent.com/01/91/0191d120-68c8-4a2f-8f45-0d3405da41e1.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/51/c6/51c6a95e-997a-4cbe-b880-2b690a578726.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/22/c0/22c0d256-dcc1-4223-878c-d607f08ec10a.jpg
http://cdn.cnetcontent.com/17/1f/171fbb28-bffc-4481-805e-0f85ae1e748e.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/a7/9c/a79cca98-b810-4219-9507-aaf87f840c58.jpg
http://cdn.cnetcontent.com/fc/9c/fc9c0fde-f2b8-4454-bc21-147f1aebdd4a.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/51/2e/512e82e7-971d-4b4e-af23-b8e7f2bcd408.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/f2/23/f223ce1e-264e-4dff-a389-549bb94cbcb4.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/6c/25/6c25a72e-c2bf-48bc-b9b0-efee54a3066a.jpg
http://cdn.cnetcontent.com/01/91/0191d120-68c8-4a2f-8f45-0d3405da41e1.jpg
http://cdn.cnetcontent.com/c8/ad/c8ad1ec2-8532-4cfd-a671-8ee1cbbd15e2.jpg
http://cdn.cnetcontent.com/6a/7e/6a7e12b1-dee9-412f-af20-2b55a37c22f9.jpg
http://cdn.cnetcontent.com/c8/ad/c8ad1ec2-8532-4cfd-a671-8ee1cbbd15e2.jpg
http://cdn.cnetcontent.com/7c/7a/7c7a9e62-ad02-4784-9f1b-c0dd904d76fd.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/77/b5/77b5902f-8569-4f1b-a963-dd0b46a8eab8.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/82/9d/829d5043-376e-4f9d-8a94-dbc2a592dcbf.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/c2/8d/c28daad0-075c-453d-9472-3ba63a89006f.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/f1/b0/f1b05f99-870b-4cdb-b61d-a92861d8e62f.jpg
http://cdn.cnetcontent.com/7d/f0/7df07083-04e4-42d2-b1d1-cdc91e5a7fb1.jpg
http://cdn.cnetcontent.com/82/6b/826bc0a2-7b97-492a-8096-6e539e404c58.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/fb/70/fb709e27-6115-41af-a2aa-3fa73bbdd074.jpg
http://cdn.cnetcontent.com/1b/53/1b53dbc6-43ba-42f7-9dab-aa08fa5a48c0.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/ef/3f/ef3fb39c-d24a-44bc-b204-40c778a16b1d.jpg
http://cdn.cnetcontent.com/e5/ec/e5ec8ff7-c4ca-4ffa-93e6-614bbfdac935.jpg
http://cdn.cnetcontent.com/01/91/0191d120-68c8-4a2f-8f45-0d3405da41e1.jpg
http://cdn.cnetcontent.com/43/87/43873ad4-b913-48ed-8a00-d6f346d51df8.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/cf/57/cf57d797-7f04-4802-a89f-1d4c1a8139c0.jpg
http://cdn.cnetcontent.com/88/97/88979ca9-75b3-4eda-a096-1378e6153225.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/ef/50/ef505420-2a53-41b3-b4ec-c80b960434cc.jpg
http://cdn.cnetcontent.com/7d/f0/7df07083-04e4-42d2-b1d1-cdc91e5a7fb1.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/94/e3/94e39d9d-32e6-472c-8c0f-8871a19463cf.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/9e/8f/9e8f3ff5-72fb-46c1-827b-90c3d8633471.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/d5/76/d57601ce-19d2-4aa7-8ea8-5b66ae9a9b15.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/6a/7e/6a7e12b1-dee9-412f-af20-2b55a37c22f9.jpg
http://cdn.cnetcontent.com/ef/3f/ef3fb39c-d24a-44bc-b204-40c778a16b1d.jpg
http://cdn.cnetcontent.com/c8/ad/c8ad1ec2-8532-4cfd-a671-8ee1cbbd15e2.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/6c/25/6c25a72e-c2bf-48bc-b9b0-efee54a3066a.jpg
http://cdn.cnetcontent.com/ec/56/ec564539-acd6-4581-992e-f41428b2c53c.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/6d/32/6d3252d9-cb0a-46d1-8b3b-d6b349743cff.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/90/47/9047009e-3fff-44bc-8de7-f08bc59de7b8.jpg
http://cdn.cnetcontent.com/df/39/df39026f-d477-4e9d-a4cb-170bd5827e5e.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/cf/57/cf57d797-7f04-4802-a89f-1d4c1a8139c0.jpg
http://cdn.cnetcontent.com/91/9f/919f9f3c-e731-4a34-9b68-97f2b773e0a5.jpg
http://cdn.cnetcontent.com/f8/24/f824bfb5-2cf1-4b1d-91af-a034887060d3.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/6c/25/6c25a72e-c2bf-48bc-b9b0-efee54a3066a.jpg
http://cdn.cnetcontent.com/f1/b0/f1b05f99-870b-4cdb-b61d-a92861d8e62f.jpg
http://cdn.cnetcontent.com/79/59/7959a704-4834-44e6-ad5f-44b2561adbaa.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/e8/e4/e8e44c10-2623-4085-b7bd-5f9395772d54.jpg
http://cdn.cnetcontent.com/85/7f/857f0879-794c-4749-a623-8dd3e1fab412.jpg
http://cdn.cnetcontent.com/d9/55/d955e385-7969-4d48-aee6-2da203238ec2.jpg
http://cdn.cnetcontent.com/31/03/3103296e-cb42-49cc-b5d6-ffaa1443153c.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/af/f4/aff40114-bfe9-4cd2-9be1-2c340e01ef39.jpg
http://cdn.cnetcontent.com/fb/70/fb709e27-6115-41af-a2aa-3fa73bbdd074.jpg
http://cdn.cnetcontent.com/cf/57/cf57d797-7f04-4802-a89f-1d4c1a8139c0.jpg
http://cdn.cnetcontent.com/f1/b0/f1b05f99-870b-4cdb-b61d-a92861d8e62f.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/e6/fa/e6fabc31-04fe-431f-85dc-552ebb7fe33e.jpg
http://cdn.cnetcontent.com/f8/66/f866364a-d0ea-4464-9fc3-64574b571dd3.jpg
http://cdn.cnetcontent.com/9a/e9/9ae97082-ea3a-4d7e-b1d9-475a62efdca5.jpg
http://cdn.cnetcontent.com/79/59/7959a704-4834-44e6-ad5f-44b2561adbaa.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/d2/ba/d2babcee-5d22-43ee-9c4f-ff7dfa9a5004.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/d5/76/d57601ce-19d2-4aa7-8ea8-5b66ae9a9b15.jpg
http://cdn.cnetcontent.com/e6/fa/e6fabc31-04fe-431f-85dc-552ebb7fe33e.jpg
http://cdn.cnetcontent.com/df/39/df39026f-d477-4e9d-a4cb-170bd5827e5e.jpg
http://cdn.cnetcontent.com/3e/f2/3ef2cc04-607e-4229-a6cd-1b7b78052dac.jpg
http://cdn.cnetcontent.com/48/79/4879ccf1-fe1f-401a-b407-9ef1245fe2ad.jpg
http://cdn.cnetcontent.com/d7/94/d794165d-eebd-4a80-a420-f7637cf52cf5.jpg
http://cdn.cnetcontent.com/e6/fa/e6fabc31-04fe-431f-85dc-552ebb7fe33e.jpg
http://cdn.cnetcontent.com/9c/27/9c2728e2-079e-4223-919a-c83620259686.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/31/03/3103296e-cb42-49cc-b5d6-ffaa1443153c.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/ed/62/ed6257e3-309a-4ae3-b7f2-273f6d5bd25e.jpg
http://cdn.cnetcontent.com/df/39/df39026f-d477-4e9d-a4cb-170bd5827e5e.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/f1/b0/f1b05f99-870b-4cdb-b61d-a92861d8e62f.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/e1/a1/e1a1d917-0697-42bf-a254-e896499594c4.jpg
http://cdn.cnetcontent.com/2b/ed/2bed4fe6-5f64-41a6-89db-4a5f91a9812f.jpg
http://cdn.cnetcontent.com/d6/0e/d60ecdb6-f3d9-4af8-9878-1673cdf4e69d.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/94/e3/94e39d9d-32e6-472c-8c0f-8871a19463cf.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/f4/c2/f4c22cc4-f834-41aa-ab81-61fe9ed2fbe7.jpg
http://cdn.cnetcontent.com/d9/96/d9969a55-9c15-4c08-9c7e-7d437c64cbbc.jpg
http://cdn.cnetcontent.com/f2/57/f2572f10-f370-4bf7-a062-e0f1830e166e.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/b2/b0/b2b0d367-03c1-4c3f-8cd1-06634f2fb470.jpg
http://cdn.cnetcontent.com/fb/70/fb709e27-6115-41af-a2aa-3fa73bbdd074.jpg
http://cdn.cnetcontent.com/6a/7e/6a7e12b1-dee9-412f-af20-2b55a37c22f9.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/6a/7e/6a7e12b1-dee9-412f-af20-2b55a37c22f9.jpg
http://cdn.cnetcontent.com/5a/41/5a416b61-0168-4fe2-8766-f84ac0b535aa.jpg
http://cdn.cnetcontent.com/d5/76/d57601ce-19d2-4aa7-8ea8-5b66ae9a9b15.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/f8/24/f824bfb5-2cf1-4b1d-91af-a034887060d3.jpg
http://cdn.cnetcontent.com/68/e3/68e34f69-dc9c-49a5-9495-2c36e3d3c5f3.jpg
http://cdn.cnetcontent.com/e9/1a/e91a8bd1-1c31-46fd-b2d4-454bc4e68d32.jpg
http://cdn.cnetcontent.com/79/59/7959a704-4834-44e6-ad5f-44b2561adbaa.jpg
http://cdn.cnetcontent.com/b2/f5/b2f5ef2a-26c6-4f74-9d6c-914d3a519cd8.jpg
http://cdn.cnetcontent.com/77/b5/77b5902f-8569-4f1b-a963-dd0b46a8eab8.jpg
http://cdn.cnetcontent.com/bf/ed/bfed6a2f-db79-4c41-b9da-6dba3dfcb4e1.jpg
http://cdn.cnetcontent.com/ce/c0/cec04275-0679-4924-95d6-2855a821bf78.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/b2/d4/b2d4e9ce-2a0a-4564-b1eb-650371ff97b9.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/a5/8a/a58ade20-63f0-48bb-aeac-3eb701c6acc0.jpg
http://cdn.cnetcontent.com/88/97/88979ca9-75b3-4eda-a096-1378e6153225.jpg
http://cdn.cnetcontent.com/e9/49/e949790f-c780-48a8-8a5d-291ad8dfb333.jpg
http://cdn.cnetcontent.com/79/4e/794eff22-2f77-4ec1-8dc3-a8c22b015697.jpg
http://cdn.cnetcontent.com/d5/76/d57601ce-19d2-4aa7-8ea8-5b66ae9a9b15.jpg
http://cdn.cnetcontent.com/d5/76/d57601ce-19d2-4aa7-8ea8-5b66ae9a9b15.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/39/0f/390f6ffe-87e2-4197-bcb5-5586fb8d8b6c.jpg
http://cdn.cnetcontent.com/77/b5/77b5902f-8569-4f1b-a963-dd0b46a8eab8.jpg
http://cdn.cnetcontent.com/f1/b0/f1b05f99-870b-4cdb-b61d-a92861d8e62f.jpg
http://cdn.cnetcontent.com/d8/76/d87630fc-6a40-4450-825f-8a330399a64a.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
http://cdn.cnetcontent.com/bb/f1/bbf16232-1b6d-4188-8a73-aa174001e127.jpg
"""


def download_file(url):
    """Download image and return local filename."""
    url_with_params_regex = re.compile(r'\?')
    if re.search(url_with_params_regex, url) is not None:
    	local_filename = path + url.split('?')[-1].split('/')[-1]
    else:
    	local_filename = path + url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename

path = 'C:\\users\\sera\\pics\\'    # path to downloaded images
urls = urls_excel.splitlines()      # splitting provided list of urls to list
urls_bad = {}                       # blank dictionary for small images
open_html = True

# analysis loop, adds small images urls to dictionary and opens them in browser
print('Starting analysis')
urls_len = len(urls)
for i, url in enumerate(urls):
    if fnmatch(url, 'Http*'):
        print('{}/{} {}'.format(i, urls_len, url))
        filename = download_file(url)
        with Image.open(filename) as im:
            if im.size < (500, 500):
                # webbrowser.open(urls[i])
                urls_bad[url] = im.size
        os.remove(filename)

# prints url - image_size
if len(urls_bad) > 0:
    print('Small images:')
    for key, size in urls_bad.items():
        print('{} - {}x{} px'.format(key, size[0], size[1]))
        badImageUrl = "".join([s.lower() for s in str(key)])
        htmlBegin = '{}{}>{}</a> - {}x{} px'.format(
            htmlUrlBegin,
            badImageUrl,
            badImageUrl,
            size[0],
            size[1]
        )
        htmlImg = '<img src={}>'.format(key)
        html += htmlParagraph + htmlBegin + htmlBr + htmlImg
else:
    print('All images are bigger than 500x500 px')
    open_html = False

if open_html:
    with open(path + 'bad_images.html', 'w') as f:
        f.write(html)

    webbrowser.open('file://{}bad_images.html'.format(path))
