import webbrowser, os, requests, fnmatch
from PIL import Image
from pprint import pprint

html = '<body bgcolor=#D3D3D3>'
htmlParagraph = '<p align=center>' 
htmlParagraphEnd = '</p>'
htmlBr = '<br>'
htmlUrlBegin = '<a href='

#copy-paste here links from MAIN IMAGE column, as is
urls_excel = """http://cdn.cnetcontent.com/1c/39/1c3985f7-92b8-4b7a-86b0-9c3c09f8a05c.jpg
http://cdn.cnetcontent.com/93/31/933194a1-e582-4dae-ae64-ad73c9799444.jpg
http://cdn.cnetcontent.com/1a/05/1a05c94b-df4e-4db2-b50a-b444fb42ceae.jpg
http://cdn.cnetcontent.com/8a/d9/8ad97739-1f8f-4935-96d8-3b42f03544b3.jpg
http://cdn.cnetcontent.com/66/ff/66ff39e0-6643-4d91-b093-041f6dfb51d3.jpg
http://cdn.cnetcontent.com/ad/b7/adb7930c-e1b5-4b79-9281-36f7c75fc4c6.jpg
http://cdn.cnetcontent.com/c7/71/c7713a6a-dd8f-4442-8d4f-4c1577db1608.jpg
http://cdn.cnetcontent.com/31/36/3136ce3a-f1c4-40e6-a64b-f10d35b0a52b.jpg
http://cdn.cnetcontent.com/e7/79/e7791360-deec-482b-9f3e-1081769fb50a.jpg
http://cdn.cnetcontent.com/2f/b6/2fb6eaa5-9383-4b45-bb26-aacec7baaa60.jpg
"""

#downloads file following provided url and returns local filename
def download_file(url):
    local_filename = path + url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk:
                f.write(chunk)
    return local_filename

path = 'C:\\users\\sera\\pics\\'    #path to downloaded images
urls = urls_excel.splitlines()      #splitting provided list of urls to list
urls_bad = {}                       #blank dictionary for small images

#analysis loop, adds small images urls to dictionary and opens them in browser
print('Starting analysis')
for i in range(len(urls)):
    if fnmatch.fnmatch(urls[i], 'Http*'):
        print(i, urls[i].split('?')[0])
        filename = download_file(urls[i].split('?')[0])
        with Image.open(filename) as im:
            urls_bad[urls[i]] = im.size
        os.remove(filename)

pprint(urls_bad)

for key, size in urls_bad.items():
    print('{} - {} x {} px'.format(key, size[0], size[1]))
    badImageUrl = "".join([s.lower() for s in str(key)])
    htmlBegin = htmlUrlBegin + badImageUrl + '>' + badImageUrl + '</a> - ' + str(size[0]) + 'x' + str(size[1]) + ' px'
    # htmlUrl = '<a href=' + key + '>'
    htmlImg = '<img src=' + key + ' width=500>'
    html += htmlParagraph + htmlBegin + htmlBr + htmlImg


with open(path + 'bad_images.html', 'w') as f:
    f.write(html)

#print(html)
webbrowser.open('file://{}bad_images.html'.format(path))
