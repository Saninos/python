import asyncio
from aiohttp import ClientSession
# If using requests > v0.13.0, use
# from grequests import async

urls = [
    'http://python-requests.org',
    'http://httpbin.org',
    'http://python-guide.org',
    'http://kennethreitz.com'
]

tasks = []

async def hello(url):
    async with ClientSession() as session:
        async with session.get(url) as response:
            response = await response.read()
            print(response)

loop = asyncio.get_event_loop()

for url in urls:
    task = asyncio.ensure_future(hello(url))
    tasks.append(task)

loop.run_until_complete(asyncio.wait(tasks))