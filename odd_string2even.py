import sys

x = str(sys.argv[1]).lower() #making all symbols lowercase
y = list(x) #making list from string

#print y

result = []

for i in y:						#deleting ' 'symbols in list
	if y[i] <> ' ':
		result.append(y[i])

y = result

"""
i = 0							alternate version with while loop

while i<len(y):
	if y[i] == ' ':
		i = i +1
	else:
		result.append(y[i])
		i = i + 1
"""

#if len(y)%2 == 0:
#	y.insert((len(y)/2),'1')



#print y

is_polindrom = False

for i in range((len(y)-1)/2):
	if y[i] == y[(len(y)-1)-i]:
		is_polindrom = True
	else:
		is_polindrom = False
		break

if is_polindrom:
	print 'YES'
else:
	print 'NO'