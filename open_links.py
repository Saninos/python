import webbrowser
from PIL import Image
import os
import requests

urls_excel = """Http://cdn.cnetcontent.com/d9/18/d918ddf9-62c6-4f6a-a016-a8124e071eed.jpg
Http://cdn.cnetcontent.com/be/30/be3033a5-2ae5-414a-ace4-2a53b2dc9a64.jpg
Http://cdn.cnetcontent.com/3d/56/3d5610bf-6953-4f72-814e-3477be8f18a3.jpg
Http://cdn.cnetcontent.com/6a/16/6a161d2c-ec89-42e4-9688-ec4d2f606c30.jpg
Http://cdn.cnetcontent.com/1e/5f/1e5f1a9f-24fe-4bb2-9c1c-548edbf94f21.jpg
Http://cdn.cnetcontent.com/4a/81/4a81b224-4cf8-499f-9be9-ce2af8ea7e22.jpg
Http://cdn.cnetcontent.com/02/58/02581f6b-4ad8-4841-b348-b716b142307d.jpg
Http://cdn.cnetcontent.com/02/58/02581f6b-4ad8-4841-b348-b716b142307d.jpg
Http://cdn.cnetcontent.com/c3/23/c323279a-8dc1-40ab-83ca-7a2442b0d7d5.jpg
Http://cdn.cnetcontent.com/0d/68/0d68233a-50b7-4e7b-92df-b3c09e5ceae7.jpg
Http://cdn.cnetcontent.com/e5/0d/e50d0f30-0d72-4df2-855a-d6649e52d3af.jpg
Http://cdn.cnetcontent.com/ca/1d/ca1d32ee-b328-4ed7-bddd-85d41111054b.jpg
Http://cdn.cnetcontent.com/71/21/712181df-3206-4969-8d85-ddd5dca73d12.jpg
Http://cdn.cnetcontent.com/c6/ee/c6eefc20-6edc-465b-a48f-40a64eb4fe6f.jpg
Http://cdn.cnetcontent.com/ff/79/ff793d42-da2e-4571-833d-77875a59aee4.jpg
Http://cdn.cnetcontent.com/40/87/40872e92-5641-475c-a491-1db2afac88cd.jpg
Http://cdn.cnetcontent.com/92/28/92282d68-f030-4aa2-a330-a13c082fecf0.jpg
"""

def download_file(url):
    local_filename = path + url.split('/')[-1]
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024): 
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
    return local_filename

path = 'C:\\users\\sera\\pics\\'
urls = urls_excel.splitlines()
urls_bad = {}

print('Starting analysis')
for i in range(len(urls)):
	print(i,urls[i])
	filename = download_file(urls[i])
	with Image.open(filename) as im:
		if im.size < (500,500):
			webbrowser.open(urls[i])
			urls_bad[urls[i]] = im.size
	os.remove(filename)

print('Small images:')
for key, size in urls_bad.items():
	print(key,' - ',size[0],'x',size[1],'px')