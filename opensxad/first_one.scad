module draw_strange_thing(sphere1_dia) {
    COEF_CUBE_SIZE = 2.3;   //some coefficients that are used for nice shape creation
    COEF_CYL_HEIGHT = 2;
    COEF_CYL_RADIUS = 6;
    COEF_SPHERE_DIAMETER = 5;
    cube_size = sphere1_dia*sqrt(COEF_CUBE_SIZE);
    cyl_h=sphere1_dia*COEF_CYL_HEIGHT;
    cyl_r=sphere1_dia/sqrt(COEF_CYL_RADIUS);
    sphere2_dia=sphere1_dia - COEF_SPHERE_DIAMETER;
    union() 
    {
        difference(){
            cube(cube_size, center=true);
            sphere(sphere1_dia);
        }
        cylinder(h=cyl_h, r=cyl_r, $fn=6);
        sphere(sphere2_dia);
    }   
}
draw_strange_thing(50);
//translate([0,0,70]) rotate ([0,0,0]) linear_extrude(height=10, center=false) square(5,center=true);