 size=20;
  roundness=8;
  height=50;
  union()
  {
    for(r=[0:90:359]) // stop *before* 360° (as 360°=0°)
      rotate([0,0,r])
        translate([size/2,size/2,0])
          cylinder(r=8,h=height, center=true);
    // same technique for the "inside" cuboids:
    for(r=[0,90])
      rotate([0,0,r])
        cube([size, size+2*roundness, height], center=true);

  }