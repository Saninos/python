brus=[100,100,1100]; //dimensions of brus: height, width

doska=[40,200,1100]; //dimensions of doska: height, width, lenght -- it's better to use another aproach
                     //otherwise I must rotate every doska and that is inconvinient
                     //I think here must be only height and width, but lenght must be applied every time I want to
                     //create a doska
                     
shelf=[1100,2000+100,5]; //dimensions of shelf: width, length, thickness
cube(brus); //front left
translate([0,2000,0]) cube(brus); //back left
translate([0,0,1100]){   //top
    cube([1100,200,40]);
    translate([0,2000-200/2,0]) cube([1100,200,40]);
}
translate([1100-100,0,0]){ //right
    cube(brus);
    translate([0,2000,0]) cube(brus);
}
translate([100,0,100]) { //bottom
    cube([1100-200,200,40]);
    translate ([0,2000-200/2,0])cube([1100-200,200,40]);  
}
color([0.3,0.2,1])translate([0,0,1100+40])cube(shelf); //desk

union(){   //X-ing on the back. todo: think about intersection
    translate([0,100,0])rotate([0,-25,90])cube([2400-180,40,200]);
    translate([0,0,1100-150])rotate([0,25,90])cube([2400-180,40,200]);
}
translate([100,0,100+40]) cube([200,2000+100,40]); //for bottom shelf, for loop needed
translate([450,0,100+40]) cube([200,2000+100,40]);
translate([800,0,100+40]) cube([200,2000+100,40]);
color([0.3,0.2,1]) translate([0,0,100+40+40]) cube(shelf); //bottom shelf

translate([0,200,1100]) cube([200,2000-300,40]); //desk-holder, for loop needed
translate([450,200,1100]) cube([200,2000-300,40]);
translate([900,200,1100]) cube([200,2000-300,40]);