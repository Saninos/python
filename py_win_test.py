import wmi
c = wmi.WMI (privileges=["Security"])

watcher = c.Win32_NTLogEvent.watch_for("creation", 2, Type="error")

while 1:
  error = watcher ()
  print("Error in %s log: %s" %  (error.Logfile, error.Message))
  # send mail to sysadmin etc.
