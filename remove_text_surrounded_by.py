import re


text = """bogdan
Section: Wireless phones
Item: Service Provider
Orange cell for https://extranet.cnetcontentsolutions.com/techspecs.aspx?skuid=82DE7223-D88B-4E57-A1E9-1BF059E1B1E3&tab=ExtendedSpecsTab


Should we use "unknown_wireless_provider_type" in case value in A[2975] is absent in TX  Sample values list?


zu
bogdan this was discussed already
...
the expression is exactly as it should be
you can go and read the guidelines
sheet 1
where the pink cell is
and how many times do i have to say pay attention to chat?


__________________________________________________________________________________




irena 
i've corrected the expression https://claims.cnetcontent.com/issues/97175 (added .Values to last row)


bogdan
have we SKU where old expression output wrong value?


irena
I think no


bogdan [2:17 PM]  
this expression operates without Values


bogdan [2:18 PM]  
uploaded an image: Capture.JPG 
Add Comment


irena [2:19 PM]  
I now it work, but i think it's not correct syntax to write it without .Values


ralitsa [2:19 PM]  
Bogdan didn't you read the syntax channel


bogdan [2:20 PM]  
ok, I see


zu [2:22 PM]  
why did you not pay attention?


bogdan [2:22 PM]  
sorry




__________________________________________________________________________________




bogdan [3:43 PM]  
https://extranet.cnetcontentsolutions.com/techspecs.aspx?skuid=12084C2A-5828-42D3-A0A8-3887F68789DD&tab=ExtendedSpecsTab


https://claims.cnetcontent.com/issues/97085 (Product Feature Bullet Point 3) 
27" big screen and 2560x1440 QHD(WQHD) high resolutionGW2765HT


why *Mfg part number* in result? without space and generally...
Is it necessary?


[3:43]  
looks not beautiful


zu [3:44 PM]  
bogdan


[3:44]  
are


[3:44]  
you


[3:44]  
reading


[3:44]  
the


[3:44]  
chat


[3:44]  
???


taras [3:44 PM]  
Alex told about this


zu [3:45 PM]  
how many times do I need to say read and pay attention to the chat? :sob: @channel


[3:45]  
:sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob: :sob:


bogdan [3:46 PM]  
ok, I found


zu [3:46 PM]  
not just found


[3:46]  
read


[3:46]  
and pay attention


alex [3:48 PM]  
:see_no_evil:


[3:48]  
I even already reported that and it's fixed


[3:49]  
will be in exports soon


bogdan [3:49 PM]  
it is from the last erxport


alex [3:50 PM]  
https://cbs.slack.com/archives/i-templex-amazon-com/p1476190142000457
 alex
will be in exports soon
Posted in #i-templex-amazon-com Oct 11th at 3:49 PM 


_________________________________________________________________________________






bogdan [12:19 PM]  
Section: "GPS, Vehicle Electronics and Accessories"
Item: "Recommended Title"
https://claims.cnetcontent.com/issues/109441


Propose add follow info to this expression
https://claims.cnetcontent.com/issues/111540 :
1. Wired or Wireless
2. Output power (coalesce Maximal or Nominal)


zu [12:20 PM]  
i want you to show a before and after title


[12:21]  
so we know what your suggestion actually looks like


bogdan [12:25 PM]  
Zu, "before and after title" - you mean before and after product type?


zu [12:25 PM]  
Bogdan


[12:25]  
what is the current name


[12:25]  
and how it would look


[12:25]  
after you change it


[12:25]  
if you change it


[12:25]  
with your changes you are suggesting


[12:25]  
add an example is what I want


________________________________________________________________________________




bogdan [9:38 AM]  
and it had better to write "wired" or "wireless" to Mouse...
How do you think?


zu [9:38 AM]  
where bogdan?


bogdan [9:39 AM]  
ZOWIE ZA11 - Mouse
ZOWIE ZA11 - Wired mouse


zu [9:39 AM]  
w h e r e


[9:39]  
whats in the box?


yordan [9:39 AM]  
leave it


zu [9:39 AM]  
go and read the description they want MINIMAL info, just what the product is


bogdan [9:39 AM]  
Product title


zu [9:39 AM]  
this is not a title


[9:39]  
oh


[9:40]  
this is what you start with


[9:40]  
i can't guess what you are talking about


_________________________________________________________________________________




bogdan [5:46 PM]  
Zu, today I stay at work during 2 hours.
What is necessary do or check during this time? I can try few variant of Suggested Product title for graphic cards.


[5:47]  
for cases when SKU.Name has different info


zu [5:48 PM]  
have we not learned it's zu AND galina


[5:49]  
you should leave it like this so  we can check it tomorrow


[5:49]  
check the whole export for mistakes/something wrong


[5:49]  
and then if you are done you can export everything with the same list and check more sections(preferably the ones with most products exported in them)


bogdan [5:50 PM]  
not only Computer Components?


galina [5:51 PM]  
Bogdan why do you hate me?


bogdan [5:51 PM]  
why do you think Galina that I hate you?)))


zu [5:52 PM]  
you can check other sections after you have checkicked everything in computer components, which i dodnt know if you will manage :smile:


bogdan [5:52 PM]  
ok)))


galina [5:53 PM]  
because Bogdan you never include me in the things you say :sob:


bogdan [5:53 PM]  
oh, sorry!!!!
I will usually include you))))


galina [5:54 PM]  
good :stuck_out_tongue:


[5:54]  
unless you usually forget :stuck_out_tongue:


bogdan [5:55 PM]  
will try to remember)


galina [5:55 PM]  
awesome! :thumbsup:


bogdan [7:06 PM]  
Section: Computer components
Item: Graphics Memory (in GB)
https://claims.cnetcontent.com/issues/121899


We have two ways to convert MB to GB:
A[241].Value.ExtractDecimals().MultiplyBy(0.000976563).ToText("F3")
A[241].Value.ExtractDecimals().First().MultiplyBy(0.000976).Round()


What do you think about second one? It gives possibility to take integer value in result.




zu [9:00 AM]  
it's a decimal


[9:00]  
have you tested this?


bogdan [9:01 AM]  
It is new export. I have tested only Computer Components in yesterday's export.


zu [9:02 AM]  
i mean your suggestion


[9:03]  
do you not have any remarks for computer components?:)


bogdan [9:03 AM]  
I see some problems in *Suggested Product title* for graphic cards. Give me some time to correct


zu [9:03 AM]  
answer the first thing first


[9:03]  
you suggested a change to graphics memory


[9:03]  
and i asked if you tested it


[9:03]  
in the parser


[9:03]  
with skus


bogdan [9:04 AM]  
yes, but SKU which I used gave normal results


zu [9:05 AM]  
S16955867


bogdan [9:05 AM]  
Yesterday I said it is necessary to check presence of Memory Type and Size in SKU.Name. Now we can see - why.
Can I add this checking?


zu [9:05 AM]  
i haven't checked it yet


[9:05]  
we are still in the first problem


[9:05]  
don't do 2 things at once


bogdan [9:07 AM]  
S16955867 is norm.
But in S16981907 we need in checking :arrow_up:
And in S16955874 or S16955878 we need use 
A[241].Value.ExtractDecimals().MultiplyBy(0.000976563).ToText("F3") instead of
A[241].Value.ExtractDecimals().First().MultiplyBy(0.000976).Round()


zu [9:08 AM]  
But in S16981907 we need in checking :arrow_up:


[9:08]  
i don;t understand this


[9:08]  
i think the first one looks best


yordan [9:09 AM]  
i think it will be a problem if turns from decimal to integer


zu [9:09 AM]  
why do you want to use the second?


[9:09]  
I agree


[9:09]  
we have some small values which will just be 0


bogdan [9:10 AM]  
"i think the first one looks best" - changed


zu [9:10 AM]  
show me the expression


bogdan [9:10 AM]  
https://claims.cnetcontent.com/issues/122760


zu [9:11 AM]  
what


[9:11]  
bogdan


[9:11]  
i am still talking about the graphics memory item


yordan [9:12 AM]  
https://claims.cnetcontent.com/issues/121899


[9:12]  
yeah about this


[9:12]  
what is the problem


zu [9:12 AM]  
why are you talking about suggested product title when i asked you to figure out the first??


bogdan [9:12 AM]  
about https://claims.cnetcontent.com/issues/121899 or about Suggested Product title?


[9:12]  
in https://claims.cnetcontent.com/issues/121899 I changed nothing - it isn't my item


yordan [9:13 AM]  
is there any problem?


zu [9:13 AM]  
you linked it and said you want to change something


bogdan [9:13 AM]  
I only ask about other way to convert MB in GB


zu [9:13 AM]  
do you see what you said last night?


bogdan [9:13 AM]  
https://cbs.slack.com/archives/i-templex-amazon-com/p1475820792000046
 bogdan
I only ask about other way to convert MB in GB
Posted in #i-templex-amazon-comOct 7th at 9:13 AM 


yordan [9:13 AM]  
what other way


zu [9:13 AM]  
look


bogdan [9:13 AM]  
A[241].Value.ExtractDecimals().MultiplyBy(0.000976563).ToText("F3") 
or
A[241].Value.ExtractDecimals().First().MultiplyBy(0.000976).Round()


zu [9:13 AM]  
nobody said the title has to be in GB


bogdan [9:14 AM]  
but you asked add " - 8GB GDDR5" and I thought it must be only GB


[9:14]  
ok


yordan [9:15 AM]  
https://cbs.slack.com/archives/i-templex-amazon-com/p1475820830000052
 bogdan
A[241].Value.ExtractDecimals().MultiplyBy(0.000976563).ToText("F3") 
or
A[241].Value.ExtractDecimals().First().MultiplyBy(0.000976).Round()
Posted in #i-templex-amazon-comOct 7th at 9:13 AM 


[9:15]  
you would need round if you want to get integer value


[9:15]  
item is decimal


bogdan [9:15 AM]  
for this reason - first


zu [9:15 AM]  
why didn't you ask if you weren't sure?


[9:15]  
just do this


[9:15]  
IF SKU.Name.Length > 60 THEN SKU.Name
ELSE SKU.Brand.Postfix(" ").ToTitleCase()_SKU.ProductLine.Postfix(" ")_SKU.ModelName.Postfix(" ")_SKU.ProductType_A[241].Value.Prefix(" - ")_A[241].Unit_A[244].Value.Prefix(" ");


bogdan [9:17 AM]  
In this item we should check presence of Memory Type and Memory Technology in SKU.Name


[9:17]  
in some cases it is presented


[9:17]  
S16983255, for example


[9:17]  
"AMD FirePro 2460 Multi-View graphics card - FirePro 2460 - 512 MB"


[9:18]  
so, we should add only Memory Technology


alex [9:18 AM]  
>S16983255, for example
is this sku from amazon request?


zu [9:18 AM]  
bogdan


bogdan [9:18 AM]  
yes


zu [9:18 AM]  
this happens because of how you wrote the expression


[9:19]  
change it to what i just said and export again please


bogdan [9:19 AM]  
can we remove Length checking and add checking of Type and Technology?


zu [9:19 AM]  
there is no sku.name.length check for the memory size and type


[9:19]  
no let's keep it that way for now


bogdan [9:21 AM]  
ok


[9:21]  
running


bogdan [9:23 AM]  
Sorry, I usually want to make any task more difficult)))
In all fields of my life :smiley:


zu [9:24 AM]  
you need to forget this here :slightly_smiling_face:


[9:24]  
the tasks here are to make everything as simple as possible


[9:24]  
otherwise it doesn't work well :wink:


bogdan [9:24 AM]  
I see :smiley:





"""

result = re.sub(r'[\(\[].*?[\)\]]','', text)
#result = re.findall(regex, text)

print(result)