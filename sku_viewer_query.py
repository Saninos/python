skus_excel = '''S16146568
S16146569
'''

skus = skus_excel.splitlines()

skusQueryList = []

for sku in skus:
	if sku is not '':
		if sku.find('s'):
			skuStr = '(ProdID=' + str(sku[1:]) + ') OR '
			skusQueryList.append(skuStr)
		else:
			skuStr = '(ProdID=' + str(sku) + ') OR'
			skusQueryList.append(skuStr)

print(''.join(skusQueryList)[:-4]) #without the last OR_space
