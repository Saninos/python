import requests
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
from requests_ntlm import HttpNtlmAuth
import re

session = requests.Session()

url = 'http://tools.cnetcontentsolutions.com/Reports/Pages/Report.aspx'
session.get(url=url, auth=HttpNtlmAuth('cnprod\\CiklumRpt', 'Djy3ErQj'), data = {'ItemPath': '/Ciklum'})

data = {
    'ItemPath': '/Ciklum/multiple_images_designers',
    'SelectedTabId': 'PropertiesTab',
    'SelectedSubTabId': 'GenericPropertiesTab'
}
stats = session.post(url=url, data=data)

juliaRegex = re.compile(r'.Piven.*')
result = juliaRegex.match(stats.text)

print(stats.status_code, stats.headers, result)
