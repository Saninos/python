import requests
from requests_ntlm import HttpNtlmAuth
import re
import logging

logging.basicConfig(filename='stats.html', level=logging.DEBUG, filemode='w')

session = requests.Session()

url = 'http://tools.cnetcontentsolutions.com/Reports/Pages/Folder.aspx'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko',
    'x-microsoftajax': 'Delta=true',
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
}

my_auth = HttpNtlmAuth(r'cnprod\CiklumRpt', 'Djy3ErQj')

parameters = {
    'ItemPath': '/Ciklum/multiple_images_designers',
    'ViewMode': 'List'
}

data = {
    'ctl04': 'ctl04|ctl31$ctl09$Reserved_AsyncLoadTarget',
    '__EVENTTARGET': 'ctl31$ctl09$Reserved_AsyncLoadTarget',
    '__VIEWSTATEGENERATOR': 'DC243524',
    'ctl31$ctl10': 'ltr',
    'ctl31$ctl11': 'quirks',
    'ctl31$AsyncWait$HiddenCancelField': 'False',
    'ctl31$ctl04$ctl03$txtValue': '6/14/2017',
    'ctl31$ctl04$ctl05$txtValue': '6/21/2017',
    'ctl31$ToggleParam$collapse': 'false',
    'ctl31$ctl07$collapse': 'false',
    'ctl31$ctl09$VisibilityState$ctl00': 'None',
    'ctl31$ctl09$ReportControl$ctl04': '100',
    '__ASYNCPOST': 'true',
}

session.get(url=url, params=parameters, auth=my_auth)

stats = session.post(
    url=url,
    auth=my_auth,
    params=parameters,
    # data=data
)

juliaRegex = re.compile(r'.*iven.*')
result = re.search(juliaRegex, stats.text)

print('{}\n{}\n{}'.format(stats.status_code, stats.headers, result))
logging.debug(stats.text)
