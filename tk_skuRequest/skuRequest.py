import tkinter as tk
import pygubu
import pyperclip
import fnmatch

class Application():
    """docstring for Application"""
    def __init__(self, master):
        self.master = master

        self.builder = pygubu.Builder()

        self.builder.add_from_file('skuRequest.ui')
        
        self.mainwindow = self.builder.get_object('skuViewerFrame', master)
        self.skuListRaw = self.builder.get_object('skuListRaw', master)
        self.skuListProcess = self.builder.get_object('skuListProcessed', master)

        self.builder.connect_callbacks(self)

    def onProcessButtonClick(self):
        self.skuListRawValue = tk.StringVar()
        self.skuListRawValue.set(self.skuListRaw.get(1.0,tk.END))

        skus_excel = self.skuListRawValue.get()

        skus = skus_excel.splitlines()

        skusQueryList = []

        for sku in skus:
            if sku is not '':
                if fnmatch.fnmatch(sku, 'S*'):
                    skuStr = '(ProdID=' + str(sku[1:]) + ') OR '
                    skusQueryList.append(skuStr)
                else:
                    skuStr = '(ProdID=' + str(sku) + ') OR '
                    skusQueryList.append(skuStr)

        query = ''.join(skusQueryList)[:-4]
        print(query)
        pyperclip.copy(query)

        self.skuListProcess.config(state=tk.NORMAL)
        self.skuListProcess.delete(1.0,tk.END)
        self.skuListProcess.insert(tk.END,query)
        self.skuListProcess.config(state=tk.DISABLED)

    def onClearButtonClick(self):
        self.skuListRaw.delete(1.0, tk.END)
        self.skuListProcess.config(state=tk.NORMAL)
        self.skuListProcess.delete(1.0, tk.END)
        self.skuListProcess.config(state=tk.DISABLED)

if __name__ == '__main__':
    root = tk.Tk()
    app = Application(root)
    root.resizable(width=False, height=False)
    root.title('SKU viewer query')
    root.mainloop()